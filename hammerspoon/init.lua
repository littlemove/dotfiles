keyBindingPrefix = {"cmd", "ctrl", "alt"}

-- Automatically reloads config
function reloadConfig(files)
  doReload = false
  for _,file in pairs(files) do
    if file:sub(-4) == ".lua" then
      doReload = true
    end
  end
  if doReload then
    hs.reload()
  end
end

-- Arrage some programs windows based on screen (laptop or external monitor)
function arrangeWindows()
  local laptopScreen = "Color LCD"
  local windowLayout = {
    {"Emacs",  nil, laptopScreen, hs.layout.maximized, nil, nil},
    {"iTerm", nil, laptopScreen, hs.layout.maximized, nil, nil},
    {"Safari", nil, laptopScreen, hs.layout.maximized, nil, nil},
    {"Mailplane 3", nil, laptopScreen, hs.layout.right50, nil, nil},
    {"Franz", nil, laptopScreen, hs.layout.left50, nil, nil}
  }
  hs.layout.apply(windowLayout)
end

hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reloadConfig):start()
hs.alert.show("Config loaded")

-- Window management
hs.hotkey.bind(keyBindingPrefix, "Left", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x
    f.y = max.y
    f.w = max.w / 2
    f.h = max.h
    win:setFrame(f)
end)

hs.hotkey.bind(keyBindingPrefix, "Right", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x + (max.w / 2)
    f.y = max.y
    f.w = max.w / 2
    f.h = max.h
    win:setFrame(f)
end)

hs.hotkey.bind(keyBindingPrefix, "f", function()
    local window = hs.window.focusedWindow()
    window:maximize()
end)

hs.hotkey.bind(keyBindingPrefix, "g", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x + (max.w * 0.05)
    f.y = max.y
    f.w = max.w - (max.w * 0.1)
    f.h = max.h
    win:setFrame(f)
end)

hs.hotkey.bind(keyBindingPrefix, "w", arrangeWindows)

-- External keyboard managment

-- Toogles between Spanish and US keyboard layout
hs.hotkey.bind(keyBindingPrefix, "k", function()
                 local currentKeyboardLayout = hs.keycodes.currentLayout()
                 if currentKeyboardLayout == "U.S. International - PC" then
                   hs.keycodes.setLayout("Spanish - ISO")
                   os.execute("'/Library/Application Support/org.pqrs/Karabiner-Elements/bin/karabiner_cli' --select-profile 'int'")
                 else
                   hs.keycodes.setLayout("U.S. International - PC")
                   os.execute("'/Library/Application Support/org.pqrs/Karabiner-Elements/bin/karabiner_cli' --select-profile 'ext'")
                 end

                 hs.alert.show(hs.keycodes.currentLayout())
end)

-- Toogle do not disturb
hs.hotkey.bind(keyBindingPrefix, "r", function()
                 local source = [[
                   tell application "System Events" to tell process "SystemUIServer"
                   key down option
                   click menu bar item 1 of menu bar 2
                   key up option
                   end tell
                   ]]

                 hs.osascript.applescript(source)
end)

-- Connect Airpods
hs.hotkey.bind(keyBindingPrefix, "a", function()
                 local source = [[
                   activate application "SystemUIServer"
                   tell application "System Events"
                   tell process "SystemUIServer"
                   -- Working CONNECT Script.  Goes through the following:
                   -- Clicks on Bluetooth Menu (OSX Top Menu Bar)
                   --    => Clicks on SX-991 Item
                   --      => Clicks on Connect Item
                   set btMenu to (menu bar item 1 of menu bar 1 whose description contains "bluetooth")
                   tell btMenu
                   click
                   tell (menu item "Diego´s AirPods" of menu 1)
                   click
                   if exists menu item "Connect" of menu 1 then
                   click menu item "Connect" of menu 1
                   else
                   click btMenu -- Close main BT drop down if Connect wasn't present
                   end if
                   end tell
                   end tell
                   end tell
                   end tell
                   ]]

                 hs.osascript.applescript(source)
end)
